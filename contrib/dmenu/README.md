`passmenu` is a [dmenu][]-based interface to [pass][], the standard Unix
password manager. This design allows you to quickly copy a password to the
clipboard without having to open up a terminal window if you don't already have
one open.

If `--type` is specified, the password is typed using [xdotool][] instead of
copied to the clipboard.

If `--otp` is specified, it will be assumed that [pass-otp][] is an installed
extension and instead of just getting the specified password, `passmenu` will
get the current one-time code.

If `--infer-otp` is specified, `passmenu` will determine whether to treat the
selected password as a regular password or an OTP secret based on a regex
pattern match. Note that the default pattern matches all password IDs, so you
probably want to use this flag in combination with `--otp-pattern`.

If `--otp-pattern REGEXP` is specified, password IDs matching the regular
expression will be interpreted as secrets for OTP-style two-factor
authentication. When used with `--otp`, only OTP password IDs will be displayed.

On wayland [dmenu-wl][] is used to replace dmenu and [ydotool][] to replace xdotool.
Note that the latter requires access to the [uinput][] device, so you'll probably
need to add an extra udev rule or similar to give certain non-root users permission.

# Usage


    passmenu [--type] [--otp] [--infer-otp] [--otp-pattern <regexp>] [dmenu arguments...]

[dmenu]: http://tools.suckless.org/dmenu/
[xdotool]: http://www.semicomplete.com/projects/xdotool/
[pass]: http://www.zx2c4.com/projects/password-store/
[dmenu-wl]: https://github.com/nyyManni/dmenu-wayland
[ydotool]: https://github.com/ReimuNotMoe/ydotool
[uinput]: https://www.kernel.org/doc/html/v4.12/input/uinput.html
[pass-otp]: https://github.com/tadfisher/pass-otp
